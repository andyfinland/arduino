#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-06-08 22:03:00

#include "Arduino.h"
#include "Arduino.h"

void setup() ;
void loop() ;
void writeEEPROM(String intelHEX) ;
void help() ;
void dumpEEPROM(unsigned int address, int bytes, bool verify) ;
void eraseEEPROM(unsigned int address, int bytes) ;
byte readByte(int address) ;
void writeByte(int address, byte data) ;
void setAddress(int address, bool outputEnable) ;
void serialEvent() ;
String getValue(String data, char separator, int index) ;
int checksum(String intelHEX) ;

#include "z80EpromV5.ino"


#endif
