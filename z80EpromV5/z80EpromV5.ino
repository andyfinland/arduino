#include "Arduino.h"

#define SHIFT_DATA 2
#define SHIFT_CLK 3
#define SHIFT_LATCH 4
#define EEPROM_D0 5
#define EEPROM_D7 12
#define WRITE_EN 13

String inputString = "";         	// a string to hold incoming data
boolean stringComplete = false;  	// whether the string is complete
char separator = ' ';				// param delimiter
int defaultBuffer = 128;			// default number bytes
int defaultEraseCheck = 64;			// report on erase progress every X bytes
int part = 0;
char buf[192];

void help();
void writeEEPROM(String intelHEX);
void eraseEEPROM(unsigned int address, int bytes);
void dumpEEPROM(unsigned int address, int bytes, bool verify);
byte readByte(int address);
void writeByte(int address, byte data);
void setAddress(int address, bool outputEnable);
void serialEvent();
int checksum(String intelHEX);
String getValue(String data, char separator, int index);

void setup() {

	// Setup shift
	pinMode(SHIFT_DATA, OUTPUT);
	pinMode(SHIFT_CLK, OUTPUT);
	pinMode(SHIFT_LATCH, OUTPUT);

	digitalWrite(WRITE_EN, HIGH);
	pinMode(WRITE_EN, OUTPUT);

	Serial.begin(57600);

	// reserve 192 bytes for the inputString:
	inputString.reserve(192);

	Serial.println("READY");

	delay(2000);
}

void loop() {

	if (stringComplete) {

			String command = getValue(inputString, separator, 0);
			String param1 = getValue(inputString, separator, 1);
			String param2 = getValue(inputString, separator, 2);

			command.trim();
			param1.trim();
			param2.trim();

			// Help
			if (command == "help" || command == "?" || command == "h") {
				help();
			}

			// Erase ROM
			else if (command == "erase" || command == "e") {
				unsigned int address = param1.toInt();
				unsigned int bytes = param2.toInt();
				if (bytes == 0)
					bytes = defaultBuffer;
				eraseEEPROM(address, bytes);
			}

			// Write to ROM
			else if (command == "write" || command == "w") {
				writeEEPROM(param1);
			}

			// Dump contents of ROM
			else if (command == "dump" || command == "d") {
				unsigned int address = param1.toInt();
				unsigned int bytes = param2.toInt();
				if (bytes == 0)
					bytes = defaultBuffer;
				dumpEEPROM(address, bytes, false);
			}

			// Verify contents of ROM
			else if (command == "verify" || command == "v") {
				unsigned int address = param1.toInt();
				unsigned int bytes = param2.toInt();
				if (bytes == 0)
					bytes = defaultBuffer;
				dumpEEPROM(address, bytes, true);
			}

			// Ready
			else if (command == "ready" || command == "r") {
				Serial.println("{\"action\":\"ready\",\"status\":\"OK\"}");
			}

			else if (command == "") {
				Serial.println();
			}

			// None of the above
			else {
				// {"error":"missing command"}
				Serial.println("{\"error\":\"missing command\"}");
			}

			// clear the string:
			inputString = "";
			command = "";
			param1 = "";
			param2 = "";
			stringComplete = false;
		}

}

/**
 * Write ROM
 */
void writeEEPROM(String intelHEX) {

	if (intelHEX.substring(0, 1) == ":" && intelHEX.length()>8) {

		/**
		 * :LLAAAATT<data>SS

		 LL is the length (1,3)
		 AAAA is the address (3,7)
		 TT is the record type
		 <data> is 'LL' of data bytes
		 SS is the checksum

		 */

		// length
		String lengthStr = intelHEX.substring(1, 3);
		char lengthArr[2];
		lengthStr.toCharArray(lengthArr, 16);
		unsigned int length = strtol(lengthArr, NULL, 16);

		// address
		String addressStr = intelHEX.substring(3, 7);
		char addressArr[4];
		addressStr.toCharArray(addressArr, 16);
		int address = strtol(addressArr, NULL, 16);
		int startAddress = address;

		// type
		String typeStr = intelHEX.substring(7, 9);
		char typeArr[2];
		typeStr.toCharArray(typeArr, 16);
		int type = strtol(typeArr, NULL, 16);

		// data
		String data = intelHEX.substring(9, intelHEX.length() - 2);

		// intel hex
		if (type == 0) {


			for (unsigned int i = 0; i < data.length(); i = i + 2) {

				String h = data.substring(i, i + 2);

				char byteArr[2];
				h.toCharArray(byteArr, 16);
				byte b = strtol(byteArr, NULL, 16);

				writeByte(address, b);

				address++;

			}

			part++;

			sprintf(buf, "{\"action\":\"write\",\"address\":%u,\"bytes\":%d,\"part\":%d,\"status\":\"progress\",\"checsum\":%d}",
						startAddress,length,part, checksum(intelHEX));
			Serial.println(buf);

			//writeStatus(startAddress, length, part, "progress", 0, "write",checksum(intelHEX));


		} else if (type == 1) {

			// EOF met :00000001FF
			Serial.println("{\"action\":\"write\",\"status\":\"done\"}");
			part = 0;

		} else {

			// Type not supported
			Serial.println("{\"error\":\"type not supported\"}");
			part = 0;

		}

	} else {
		Serial.println("{\"action\":\"write\",\"status\":\"empty\"}");
	}

}

/**
 * Help
 */
void help() {
	Serial.println("\nAW EPROM Programmer V4\n");
	Serial.println("(h)elp/?");
	Serial.println("(r)eady");
	Serial.println("(w)rite <IntelHex>");
	Serial.println("(d)ump <address> <bytes>");
	Serial.println("(e)rase <address> <bytes>");
	Serial.println();
}

/**
 * Dump ROM
 */
void dumpEEPROM(unsigned int address, int bytes, bool verify) {

	// {"action":"dump/verify","address":0,"bytes":2,"data":[255,255],"status":"OK"}

	Serial.print("{\"action\":\"");
	if (verify == true) {
		Serial.print("verify");
	} else {
		Serial.print("dump");
	}

	Serial.print ("\"");
	Serial.print(",\"address\":");
	Serial.print(address);
	Serial.print(",\"bytes\":");
	Serial.print(bytes);
	Serial.print(",\"data\":[");

	for (int base = 0; base < bytes; base++) {

		Serial.print(readByte(address + base));
		if (base < bytes - 1)
			Serial.print(",");
	}

	Serial.print("],\"status\":\"OK\"");
	Serial.println("}");
	Serial.flush();
}

/**
 * Erase ROM
 */
void eraseEEPROM(unsigned int address, int bytes) {

	// {"action":"erase","address":0,"bytes":64,"part":1,"status":"progress|done","total":512}

	int c = 0;
	int part = 1;
	unsigned int i;

	for (i = address; i < address + bytes; i++) {
		writeByte(i, 0xff);
		if (c==defaultEraseCheck) {
			// progress
			sprintf(buf, "{\"action\":\"erase\",\"address\":%u,\"bytes\":%d,\"part\":%d,\"status\":\"progress\",\"total\":%d}",
					(i - defaultEraseCheck),c,part,bytes);
			Serial.println(buf);
			c = 0;
			part++;
		}
		c++;
	}

	// done
	sprintf(buf, "{\"action\":\"erase\",\"address\":%u,\"bytes\":%d,\"part\":%d,\"status\":\"done\"}",
			((part - 1) * defaultEraseCheck) ,c, part);
	Serial.println(buf);

	Serial.flush();
}


/*
 * Read byte
 */
byte readByte(int address) {

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		pinMode(pin, INPUT);
	}
	setAddress(address, /*outputEnable*/true);

	byte data = 0;
	for (int pin = EEPROM_D7; pin >= EEPROM_D0; pin -= 1) {
		data = (data << 1) + digitalRead(pin);
	}
	return data;
}

/*
 * Write Byte
 */
void writeByte(int address, byte data) {

	setAddress(address, /*outputEnable*/false);
	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		pinMode(pin, OUTPUT);
	}

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		digitalWrite(pin, data & 1);
		data = data >> 1;
	}

	digitalWrite(WRITE_EN, LOW);
	delayMicroseconds(1);
	digitalWrite(WRITE_EN, HIGH);
	delay(10);

}

/*
 * Output address
 */
void setAddress(int address, bool outputEnable) {

	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST,
			(address >> 8) | (outputEnable ? 0x00 : 0x80));
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address);

	digitalWrite(SHIFT_LATCH, LOW);
	digitalWrite(SHIFT_LATCH, HIGH);
	digitalWrite(SHIFT_LATCH, LOW);
}

/**
 * Serial Event
 */
void serialEvent() {
	while (Serial.available()) {

		char inChar = (char) Serial.read();

		inputString += inChar;

		if (inChar == '\n' || inChar == '\r') {
			stringComplete = true;
		}
	}
}

/**
 * Get parameters
 */
String getValue(String data, char separator, int index) {
	int found = 0;
	int strIndex[] = { 0, -1 };
	int maxIndex = data.length() - 1;

	for (int i = 0; i <= maxIndex && found <= index; i++) {
		if (data.charAt(i) == separator || i == maxIndex) {
			found++;
			strIndex[0] = strIndex[1] + 1;
			strIndex[1] = (i == maxIndex) ? i + 1 : i;
		}
	}

	return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

/**
 * Calculate checksum
 */
int checksum(String intelHEX) {

	String hexStr = intelHEX.substring(1, intelHEX.length()-2);
	String checkStr = intelHEX.substring(intelHEX.length() - 2, intelHEX.length());

	// checksum
	char checkArr[2];
	checkStr.toCharArray(checkArr, 16);
	unsigned int checksum = strtol(checkArr, NULL, 16);

	unsigned int chk = 0;

	for (unsigned int i = 0; i < hexStr.length(); i = i + 2) {

		String h = hexStr.substring(i, i + 2);
		char byteArr[2];
		h.toCharArray(byteArr, 16);
		unsigned int b = strtol(byteArr, NULL, 16);

		chk = chk + b;
	}

	chk = (chk  ^ 0xff) & 0xff;
	chk = chk + 1;

	if (checksum == chk) {
		return 1;
	} else {
		return 0;
	}
}
