#include "Arduino.h"
#include "MemoryFree.h"

// Width in bits of address bus (2K=11, 4K=12, 8K=13, 16K=14, 32K=15, 64K=16)
const int ADDR_BUS_WIDTH = 15;
const int ROM_SIZE = 0x8000;

// AT28C256 (32K): WE=27, OE=22, CE=20
// AT28C16 (2K) : WE=21, OE=20, CE=18

// Chip Enable
const int CE = 32;
// Ouput Enable
const int OE = 28;
// Write Enable
const int WE = 26;

// address lines of EEPROM. Change to accomodate your setup
int dbit0 = 39;
int dbit1 = 41;
int dbit2 = 43;
int dbit3 = 42;
int dbit4 = 44;
int dbit5 = 49;
int dbit6 = 47;
int dbit7 = 45;

int abit0 = 37;
int abit1 = 35;
int abit2 = 33;
int abit3 = 31;
int abit4 = 29;
int abit5 = 27;
int abit6 = 25;
int abit7 = 23;
int abit8 = 22;
int abit9 = 24;
int abit10 = 30;

// 32k (256)
int abit11 = 40;
int abit12 = 34;
int abit13 = 38;
int abit14 = 36;

// 64K (512)
int abit15 = 27;

// Data bus pins
const int D[] = { dbit0, dbit1, dbit2, dbit3, dbit4, dbit5, dbit6, dbit7 };

// Address bus pins
const int A[] = { abit0, abit1, abit2, abit3, abit4, abit5, abit6, abit7, abit8,
		abit9, abit10, abit11, abit12, abit13, abit14, abit15 };

/**
 * Functions
 */

void eraseROM(unsigned int address, int bytes);
void fastErase();
void disableSoftwareWriteProtect();
void enableSoftwareWriteProtect();
int checksum(String intelHEX) ;
void writeROM(String intelHEX);
void dumpROM(unsigned int address, int bytes, bool verify);
void writeStatus(unsigned int address, int bytes, int part, String progress,
		int total, String action, int checksum);
void writeByte(int address, byte data);
void setEraseByte(byte value, unsigned int address);
void setProtectByte(byte value, unsigned int address);
byte readByte(unsigned int address);
void setDataBus(byte val);
byte readDataBus();
void setAddress(unsigned int addr);
void setDataBusMode(int mode);
void printHex(int num, int precision);
void help();
void serialEvent();
String getValue(String data, char separator, int index);

/**
 * Variables
 */
String inputString = "";         	// a string to hold incoming data
boolean stringComplete = false;  	// whether the string is complete
char separator = ' ';				// param delimiter
int defaultBuffer = 128;			// default number bytes
int defaultEraseCheck = 64;			// report on erase progress every X bytes
int part = 0;
extern unsigned int __bss_end;		// mem
extern void *__brkval;				// mem

/**
 * Setup
 */
void setup() {

	Serial.begin(9600);

	// Configure control pins
	pinMode(CE, OUTPUT);
	pinMode(WE, OUTPUT);
	pinMode(OE, OUTPUT);

	// set in standby mode
	digitalWrite(OE, HIGH);
	digitalWrite(WE, HIGH);
	digitalWrite(CE, HIGH);

	// set the address bus for output
	for (int i = 0; i < ADDR_BUS_WIDTH; i++) {
		pinMode(A[i], OUTPUT);
	}

	// reserve 256 bytes for the inputString:
	inputString.reserve(256);

	Serial.println("READY");

	delay(2000);

}

void loop() {

	if (stringComplete) {

		String command = getValue(inputString, separator, 0);
		String param1 = getValue(inputString, separator, 1);
		String param2 = getValue(inputString, separator, 2);

		command.trim();
		param1.trim();
		param2.trim();

		// Help
		if (command == "help" || command == "?" || command == "h") {
			help();
		}

		// Erase ROM
		else if (command == "erase" || command == "e") {
			unsigned int address = param1.toInt();
			unsigned int bytes = param2.toInt();
			if (bytes == 0)
				bytes = defaultBuffer;
			eraseROM(address, bytes);
		}

		// Fast Erase
		else if (command == "fasterase" || command == "fa") {
			fastErase();
		}

		// Enable software protect
		else if (command == "protect" || command == "pr") {
			enableSoftwareWriteProtect();
		}

		// Disable software protect
		else if (command == "disable" || command == "di") {
			disableSoftwareWriteProtect();
		}

		// Write to ROM
		else if (command == "write" || command == "w") {
			writeROM(param1);
		}

		// Dump contents of ROM
		else if (command == "dump" || command == "d") {
			unsigned int address = param1.toInt();
			unsigned int bytes = param2.toInt();
			if (bytes == 0)
				bytes = defaultBuffer;
			dumpROM(address, bytes, false);
		}

		// Verify contents of ROM
		else if (command == "verify" || command == "v") {
			unsigned int address = param1.toInt();
			unsigned int bytes = param2.toInt();
			if (bytes == 0)
				bytes = defaultBuffer;
			dumpROM(address, bytes, true);
		}

		// Ready
		else if (command == "ready" || command == "r") {
			Serial.println("{\"action\":\"ready\",\"status\":\"OK\"}");
		}

		// Memory
		else if (command == "memory" || command == "m") {
			Serial.print("{\"action\":\"memory\",\"size\":");
			Serial.print(freeMemory());
			Serial.println("}");
		}

		else if (command == "") {
			Serial.println();
		}

		// None of the above
		else {
			// {"error":"missing command"}
			Serial.println("{\"error\":\"missing command\"}");
		}

		// clear the string:
		inputString = "";
		command = "";
		param1 = "";
		param2 = "";
		stringComplete = false;
	}

}

/**
 * Help
 */
void help() {
	Serial.println("\nAW EPROM Programmer V4\n");
	Serial.println("(h)elp/?");
	Serial.println("(r)eady");
	Serial.println("(m)emory");
	Serial.println("(w)rite <IntelHex>");
	Serial.println("(d)ump <address> <bytes>");
	Serial.println("(v)erify <address> <bytes>");
	Serial.println("(e)rase <address> <bytes>");
	Serial.println("(fa)st erase");
	Serial.println("(pr)otect");
	Serial.println("(di)sable");
	Serial.println();
}

/**
 * Disable Write Protect 28C256
 */
void disableSoftwareWriteProtect() {

	digitalWrite(OE, LOW);
	digitalWrite(WE, HIGH);
	digitalWrite(CE, LOW);

	setDataBusMode(INPUT);

	byte test = readByte(0);

	digitalWrite(OE, HIGH);
	digitalWrite(CE, HIGH);

	setDataBusMode(OUTPUT);

	setProtectByte(0xaa, 0x5555);
	setProtectByte(0x55, 0x2aaa);
	setProtectByte(0x80, 0x5555);
	setProtectByte(0xaa, 0x5555);
	setProtectByte(0x55, 0x2aaa);
	setProtectByte(0x20, 0x5555);	// note 20

	setProtectByte(0x00, 0x00);

	digitalWrite(CE, LOW);
	delay(1);
	setDataBusMode(INPUT);

	Serial.println("{\"action\":\"disableProtect\",\"status\":\"done\"}");
}

/**
 * Enable Write Protect 28C256
 */
void enableSoftwareWriteProtect() {

	digitalWrite(OE, LOW);
	digitalWrite(WE, HIGH);
	digitalWrite(CE, LOW);

	setDataBusMode(INPUT);

	byte test = readByte(0);

	digitalWrite(OE, HIGH);
	digitalWrite(CE, HIGH);

	setDataBusMode(OUTPUT);

	setProtectByte(0xaa, 0x5555);
	setProtectByte(0x55, 0x2aaa);
	setProtectByte(0xa0, 0x5555);

	setProtectByte(0x00, 0x00);

	digitalWrite(CE, LOW);
	delay(1);
	setDataBusMode(INPUT);

	Serial.println("{\"action\":\"enableProtect\",\"status\":\"done\"}");
}

/**
 * used for software protection
 */
void setProtectByte(byte value, unsigned int address) {
	setAddress(address);
	setDataBus(value);

	digitalWrite(CE, LOW);
	digitalWrite(WE, LOW);

	delay(1);

	digitalWrite(WE, HIGH);
	digitalWrite(CE, HIGH);

}

/**
 * Perform fast chip erase
 */
void fastErase() {

	setDataBusMode(OUTPUT);

	digitalWrite(CE, LOW);

	setEraseByte(0xaa, 0x5555);
	setEraseByte(0x55, 0x2aaa);
	setEraseByte(0x80, 0x5555);
	setEraseByte(0xaa, 0x5555);
	setEraseByte(0x55, 0x2aaa);
	setEraseByte(0x10, 0x5555);	// note 10

	delay(100);

	setDataBusMode(INPUT);

	digitalWrite(CE, HIGH);

	Serial.println("{\"action\":\"fasterase\",\"status\":\"done\"}");
}

/**
 * Used for fast erase
 */
void setEraseByte(byte value, unsigned int address) {

	digitalWrite(OE, HIGH);
	digitalWrite(WE, HIGH);

	setAddress(address);
	setDataBus(value);

	digitalWrite(WE, LOW);
	delayMicroseconds(1);
	digitalWrite(WE, HIGH);
}

/**
 * Erase ROM
 */
void eraseROM(unsigned int address, int bytes) {

	// {"action":"erase","address":0,"bytes":64,"part":1,"status":"progress|done","total":512,"checksum":1}

	int c = 0;
	int part = 1;
	unsigned int i;

	for (i = address; i < address + bytes; i++) {
		writeByte(i, 0xFF);

		if (c == defaultEraseCheck) {
			writeStatus(i - defaultEraseCheck, c, part, "progress", bytes,
					"erase",1);
			c = 0;
			part++;
		}

		c++;
	}

	// completed
	writeStatus(((part - 1) * defaultEraseCheck) + address, c, part, "done",
			bytes, "erase",1);

	Serial.flush();

}

/**
 * Update write progress
 */
void writeStatus(unsigned int address, int bytes, int part, String progress,
		int total, String action, int checksum) {

	Serial.print("{\"action\":\"");
	Serial.print(action);
	Serial.print("\"");
	Serial.print(",\"address\":");
	Serial.print(address);
	Serial.print(",\"bytes\":");
	Serial.print(bytes);
	Serial.print(",\"part\":");
	Serial.print(part);
	Serial.print(",\"status\":\"");
	Serial.print(progress);
	Serial.print("\",\"total\":");
	Serial.print(total);
	Serial.print(",\"checksum\":");
	Serial.print(checksum);
	Serial.println("}");

}



/**
 * Load Intel HEX
 */
void writeROM(String intelHEX) {

	if (intelHEX.substring(0, 1) == ":" && intelHEX.length()>8) {

		/**
		 * :LLAAAATT<data>SS

		 LL is the length (1,3)
		 AAAA is the address (3,7)
		 TT is the record type
		 <data> is 'LL' of data bytes
		 SS is the checksum

		 */

		// length
		String lengthStr = intelHEX.substring(1, 3);
		char lengthArr[2];
		lengthStr.toCharArray(lengthArr, 16);
		unsigned int length = strtol(lengthArr, NULL, 16);

		// address
		String addressStr = intelHEX.substring(3, 7);
		char addressArr[4];
		addressStr.toCharArray(addressArr, 16);
		int address = strtol(addressArr, NULL, 16);
		int startAddress = address;

		// type
		String typeStr = intelHEX.substring(7, 9);
		char typeArr[2];
		typeStr.toCharArray(typeArr, 16);
		int type = strtol(typeArr, NULL, 16);

		// data
		String data = intelHEX.substring(9, intelHEX.length() - 2);


		// intel hex
		if (type == 0) {

			for (unsigned int i = 0; i < data.length(); i = i + 2) {

				String h = data.substring(i, i + 2);

				char byteArr[2];
				h.toCharArray(byteArr, 16);
				byte b = strtol(byteArr, NULL, 16);

				writeByte(address, b);

				address++;

			}

			part++;

			writeStatus(startAddress, length, part, "progress", 0, "write",checksum(intelHEX));

		}

		// EOF met :00000001FF

		else if (type == 1) {

			Serial.println("{\"action\":\"write\",\"status\":\"done\"}");
			part = 0;

		} else {

			Serial.println("{\"error\":\"type not supported\"}");
			part = 0;

		}

	} else {
		Serial.println("{\"action\":\"write\",\"status\":\"empty\"}");
	}

}



/**
 * Calculate checksum
 */
int checksum(String intelHEX) {

	String hexStr = intelHEX.substring(1, intelHEX.length()-2);
	String checkStr = intelHEX.substring(intelHEX.length() - 2, intelHEX.length());

	// checksum
	char checkArr[2];
	checkStr.toCharArray(checkArr, 16);
	int checksum = strtol(checkArr, NULL, 16);

	unsigned int chk = 0;

	for (unsigned int i = 0; i < hexStr.length(); i = i + 2) {

		String h = hexStr.substring(i, i + 2);
		char byteArr[2];
		h.toCharArray(byteArr, 16);
		unsigned int b = strtol(byteArr, NULL, 16);

		chk = chk + b;
	}

	chk = (chk  ^ 0xff) & 0xff;
	chk = chk + 1;

	if (checksum == chk) {
		return 1;
	} else {
		return 0;
	}
}

// print HEX
void printHex(int num, int precision) {
	char tmp[16];
	char format[128];

	sprintf(format, "%%.%dX", precision);
	sprintf(tmp, format, num);
	Serial.print(tmp);
}

/*
 * Write a byte to the EEPROM at the specified address.
 */
void writeByte(int address, byte data) {

	setAddress(address);

	digitalWrite(OE, HIGH);
	digitalWrite(CE, LOW);
	digitalWrite(WE, HIGH);

	setDataBusMode(OUTPUT);
	setDataBus(data);

	digitalWrite(WE, LOW);
	delayMicroseconds(1);
	digitalWrite(WE, HIGH);

	delay(20);

	// data polling
	/*unsigned long startTime = millis();
	setDataBusMode(INPUT);
	while (data != readDataBus()) {
		if (millis() - startTime > 3000)
			return false;
	};*/
}

/*
 * Demp the contents of the EEPROM and print them to the serial monitor.
 */
void dumpROM(unsigned int address, int bytes, bool verify) {

	// {"action":"dump/verify","address":0,"bytes":2,"data":[255,255],"status":"OK"}



	Serial.print("{\"action\":\"");
	if (verify == true) {
		Serial.print("verify");
	} else {
		Serial.print("dump");
	}

	Serial.print ("\"");
	Serial.print(",\"address\":");
	Serial.print(address);
	Serial.print(",\"bytes\":");
	Serial.print(bytes);
	Serial.print(",\"data\":[");

	for (int base = 0; base < bytes; base++) {

		Serial.print(readByte(address + base));
		if (base < bytes - 1)
			Serial.print(",");
	}

	Serial.print("],\"status\":\"OK\"");
	Serial.println("}");
	Serial.flush();
}

/*
 * Read a byte from the EEPROM at the specified address.
 */
byte readByte(unsigned int address) {

	setAddress(address);

	digitalWrite(OE, LOW);
	digitalWrite(CE, LOW);
	digitalWrite(WE, HIGH);

	setDataBusMode(INPUT);

	return readDataBus();

}

// set the data bus
void setDataBus(byte val) {
	for (int i = 0; i < 8; i++) {
		int a = bitRead(val, i);
		digitalWrite(D[i], a);
	}
}

// set the address bus
void setAddress(unsigned int addr) {
	for (int i = 0; i < ADDR_BUS_WIDTH; i++) {
		int a = bitRead(addr, i);
		digitalWrite(A[i], a);
	}
}

// Set data bus to INPUT or OUTPUT
void setDataBusMode(int mode) {
	for (int i = 0; i < 8; i++) {
		pinMode(D[i], mode);
	}
}

// read the databus
byte readDataBus() {

	byte data = 0;

	for (int i = 0; i < 8; i++) {
		int d = digitalRead(D[i]);
		data += (d << i);
	}

	return data;

}

/**
 * Catch the commands
 */
void serialEvent() {
	while (Serial.available()) {
		// get the new byte:
		char inChar = (char) Serial.read();
		// add it to the inputString:
		inputString += inChar;

		if (inChar == '\n') {
			stringComplete = true;
		}
	}
}

/**
 * Get parameters
 */
String getValue(String data, char separator, int index) {
	int found = 0;
	int strIndex[] = { 0, -1 };
	int maxIndex = data.length() - 1;

	for (int i = 0; i <= maxIndex && found <= index; i++) {
		if (data.charAt(i) == separator || i == maxIndex) {
			found++;
			strIndex[0] = strIndex[1] + 1;
			strIndex[1] = (i == maxIndex) ? i + 1 : i;
		}
	}

	return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

// get free memory
int freeMemory() {
	int free_memory;

	if ((int) __brkval == 0)
		free_memory = ((int) &free_memory) - ((int) &__bss_end);
	else
		free_memory = ((int) &free_memory) - ((int) __brkval);

	return free_memory;
}

