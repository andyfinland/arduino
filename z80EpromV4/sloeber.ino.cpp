#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-06-08 21:56:09

#include "Arduino.h"
#include "Arduino.h"
#include "MemoryFree.h"

void setup() ;
void loop() ;
void help() ;
void disableSoftwareWriteProtect() ;
void enableSoftwareWriteProtect() ;
void setProtectByte(byte value, unsigned int address) ;
void fastErase() ;
void setEraseByte(byte value, unsigned int address) ;
void eraseROM(unsigned int address, int bytes) ;
void writeStatus(unsigned int address, int bytes, int part, String progress, 		int total, String action, int checksum) ;
void writeROM(String intelHEX) ;
int checksum(String intelHEX) ;
void printHex(int num, int precision) ;
void writeByte(int address, byte data) ;
void dumpROM(unsigned int address, int bytes, bool verify) ;
byte readByte(unsigned int address) ;
void setDataBus(byte val) ;
void setAddress(unsigned int addr) ;
void setDataBusMode(int mode) ;
byte readDataBus() ;
void serialEvent() ;
String getValue(String data, char separator, int index) ;
int freeMemory() ;

#include "z80EpromV4.ino"


#endif
