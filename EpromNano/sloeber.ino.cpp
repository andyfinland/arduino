#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-06-06 23:24:17

#include "Arduino.h"
#include "Arduino.h"

void setup() ;
void loop() ;
void writeEEPROM() ;
void showHelp() ;
void dumpEEPROM() ;
void eraseEEPROM() ;
byte readByte(int address) ;
void writeByte(int address, byte data) ;
void setAddress(int address, bool outputEnable) ;
void serialEvent() ;
void freeMemory() ;

#include "EpromNano.ino"


#endif
