#include "Arduino.h"

#define SHIFT_DATA 2
#define SHIFT_CLK 3
#define SHIFT_LATCH 4
#define EEPROM_D0 5
#define EEPROM_D7 12
#define WRITE_EN 13

String inputString = "";
boolean stringComplete = false;
extern unsigned int __bss_end;
extern void *__brkval;

byte data[] = { 0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b,
		0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47 };

void setup() {

	// Setup shift
	pinMode(SHIFT_DATA, OUTPUT);
	pinMode(SHIFT_CLK, OUTPUT);
	pinMode(SHIFT_LATCH, OUTPUT);

	digitalWrite(WRITE_EN, HIGH);
	pinMode(WRITE_EN, OUTPUT);

	Serial.begin(9600);

	Serial.println("READY");

	showHelp();
}

void loop() {

	if (stringComplete) {

		if (inputString.substring(0, 1) == "h") {
			showHelp();
		}

		if (inputString.substring(0, 1) == "d") {
			dumpEEPROM();
		}

		if (inputString.substring(0, 1) == "w") {
			writeEEPROM();
		}

		if (inputString.substring(0, 1) == "e") {
			eraseEEPROM();
		}

		if (inputString.substring(0, 1) == "m") {
			freeMemory();
		}

		inputString = "";
		stringComplete = false;
	}

}

/**
 * Write ROM
 */
void writeEEPROM() {

	Serial.print("Programming EEPROM");
	for (unsigned int address = 0; address < sizeof(data); address += 1) {
		writeByte(address, data[address]);

		if (address % 64 == 0) {
			Serial.print(".");
		}
	}
	Serial.println(" done");
}

void showHelp() {

	Serial.println("\nEEPROM Loader v1.0\n");
	Serial.println("(h) Help");
	Serial.println("(e) Erase");
	Serial.println("(d) Dump");
	Serial.println("(w) Write");
	Serial.println("(w) Memory");
	Serial.println();
}

/**
 * Dump ROM
 */
void dumpEEPROM() {

	Serial.println("Dumping EEPROM");

	for (int base = 0; base <= 255; base += 16) {
		byte data[16];
		for (int offset = 0; offset <= 15; offset += 1) {
			data[offset] = readByte(base + offset);
		}

		char buf[80];
		sprintf(buf,
				"%03x:  %02x %02x %02x %02x %02x %02x %02x %02x   %02x %02x %02x %02x %02x %02x %02x %02x",
				base, data[0], data[1], data[2], data[3], data[4], data[5],
				data[6], data[7], data[8], data[9], data[10], data[11],
				data[12], data[13], data[14], data[15]);

		Serial.println(buf);
	}
	Serial.println(" done");
}

/**
 * Erase ROM
 */
void eraseEEPROM() {

	Serial.print("Erasing EEPROM");
	for (int address = 0; address <= 1023; address += 1) {

		writeByte(address, 0xff);

		if (address % 64 == 0) {
			Serial.print(".");
		}
	}
	Serial.println(" done");
}

/*
 * Read byte
 */
byte readByte(int address) {

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		pinMode(pin, INPUT);
	}
	setAddress(address, /*outputEnable*/true);

	byte data = 0;
	for (int pin = EEPROM_D7; pin >= EEPROM_D0; pin -= 1) {
		data = (data << 1) + digitalRead(pin);
	}
	return data;
}

/*
 * Write Byte
 */
void writeByte(int address, byte data) {

	setAddress(address, /*outputEnable*/false);
	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		pinMode(pin, OUTPUT);
	}

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
		digitalWrite(pin, data & 1);
		data = data >> 1;
	}

	digitalWrite(WRITE_EN, LOW);
	delayMicroseconds(1);
	digitalWrite(WRITE_EN, HIGH);
	delay(10);

}

/*
 * Output address
 */
void setAddress(int address, bool outputEnable) {

	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST,
			(address >> 8) | (outputEnable ? 0x00 : 0x80));
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address);

	digitalWrite(SHIFT_LATCH, LOW);
	digitalWrite(SHIFT_LATCH, HIGH);
	digitalWrite(SHIFT_LATCH, LOW);
}

/** Catch serial **/
void serialEvent() {
	while (Serial.available()) {

		char inChar = (char) Serial.read();

		inputString += inChar;

		if (inChar == '\n' || inChar == '\r') {
			stringComplete = true;
		}
	}
}

// get free memory
void freeMemory() {
	int free_memory;

	if ((int) __brkval == 0)
		free_memory = ((int) &free_memory) - ((int) &__bss_end);
	else
		free_memory = ((int) &free_memory) - ((int) __brkval);

	Serial.println(free_memory);
}
